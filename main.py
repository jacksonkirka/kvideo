from kivy.app import App
from kivy.uix.video import Video
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.filechooser import FileChooser
from kivy.uix.boxlayout import BoxLayout

class KVideo(FloatLayout):

    def select(self, filename):
        try:
            self.k.ids.kivyvideo.source = filename[0]
        except:
            pass

class KVideoApp(App):
    def build(self):
        self.k = KVideo()
        self.p = 0
        return self.k

    def play(self):
        self.k.ids.kivyvideo.state = 'play'

    def stop_video(self):
        self.k.ids.kivyvideo.state = 'stop'

    def forward(self):
        self.p += .10
        self.k.ids.kivyvideo.seek(self.p)

    def rewind(self):
        self.p -= .10
        self.k.ids.kivyvideo.seek(self.p)

    def pause(self):
        self.k.ids.kivyvideo.state = 'pause'

    def select(self, filename):
        try:
            self.k.ids.kivyvideo.source = filename[0]
        except:
            pass

if __name__ == '__main__':
    KVideoApp().run()
